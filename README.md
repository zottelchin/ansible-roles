# ansible-roles

This repository contains all Ansible roles to set up a Debian 9 server for comfortable administration using Docker Compose.

## Example Playbook

```yaml
- name: Set up everything
  hosts: all
  roles:
    - role: essentials
      tags: [essentials]
      vars:
        permit_root_login: "no"
    - role: user
      tags: [essentials, user]
      vars:
        username: "..."
        password: "..." # mkpasswd --method=sha-512
        authorized_keys: |
          ssh-ed25519 ...
    - role: iptables
      tags: [essentials, iptables]
    - role: docker
      tags: [docker]
    - role: data-backup
      tags: [backup]
      vars:
        restic: |
          export RESTIC_REPOSITORY='b2:somewhere:{{ ansible_facts['nodename'] }}'
          export RESTIC_PASSWORD='...'
          export B2_ACCOUNT_ID='...'
          export B2_ACCOUNT_KEY='...'
```

You can run this with the following commands:
```
# clone ansible-roles
git clone https://codeberg.org/momar/ansible-roles.git roles
# edit the hosts file
vi hosts
# run the playbook
ansible-playbook -i hosts <filename.yml> -Kbu <username-or-root>
```

## Roles

### essentials
Install packages for easier administration of the system, including fancy tools like [micro](https://github.com/zyedidia/micro) and [hexyl](https://github.com/sharkdp/hexyl), and a comfortable zshrc environment.

**WARNING: This disables password authentication for SSH, so you better make sure that you have a key set up.**

If you're also creating a user, use the following to disable root login via SSH:
```yaml
roles:
- role: essentials
    vars:
      permit_root_login: no
```


### user
Create a user with sudo rights, an SSH key and an authorized_keys file:
```yaml
roles:
- role: user
    vars:
      username: "..."
      password: "..." # mkpasswd --method=sha-512
      authorized_keys: |
        ssh-ed25519 ...
```

### iptables
Create a default iptables configuration and use `/data/@$HOSTNAME/services.rules` for additional rules.

### docker
Install Docker and Docker Compose, make containers reachable via `containername.docker` using /etc/hosts, and set up Traefik with its configuration in `/data/@$HOSTNAME/traefik.toml`.

After changing `/data/@$HOSTNAME/traefik.toml`, you can run `sudo docker restart traefik" to apply the new configuration.
After changing `/data/@$HOSTNAME/traefik.env`, you need to run `sudo docker rm --force traefik` to remove the traefik container, and run `ansible-playbook -i hosts <filename.yml> -Kbu <username-or-root> -t docker` on your local machine to re-apply the playbook.

### db-backup
Backup databases defined in `/data/*/database-backup/databases` every 6 hours, keep backups for 7 days.

Full backup logs can be requested with `sudo journalctl -u data-backup`.

### data-backup
Backup `/data` with restic to an offsite location. Includes `db-backup`. Example:
```yaml
roles:
- role: data-backup
    vars:
      restic: |
        export RESTIC_REPOSITORY='b2:somewhere:{{ ansible_facts['nodename'] }}'
        export RESTIC_PASSWORD='...'
        export B2_ACCOUNT_ID='...'
        export B2_ACCOUNT_KEY='...'
```

# TODO:
- determine correct postgres version for pg_dump
- send logs to logstash
  - systemd
    - https://www.elastic.co/guide/en/logstash/6.5/plugins-inputs-syslog.html
    - https://raymii.org/s/tutorials/Syslog_config_for_remote_logservers_for_syslog-ng_and_rsyslog_client_server.html
  - docker
    - https://logz.io/blog/logz-io-docker-log-collector/
    - https://github.com/gliderlabs/logspout
    - https://github.com/looplab/logspout-logstash
- dynamic-dns
- raid
- gluster
