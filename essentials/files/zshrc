READNULLCMD=${PAGER:-/usr/bin/pager}

# An array to note missing features to ease diagnosis in case of problems.
typeset -ga debian_missing_features

zstyle ':completion:*:sudo:*' command-path /usr/local/sbin \
                                           /usr/local/bin  \
                                           /usr/sbin       \
                                           /usr/bin        \
                                           /sbin           \
                                           /bin            \
                                           /usr/X11R6/bin

(( ${+aliases[run-help]} )) && unalias run-help
autoload -Uz run-help



[[ -o login ]] && source /etc/profile
source /opt/antigen.zsh

antigen use oh-my-zsh

antigen bundles <<EOBUNDLES

extract  # Helper for extracting different types of archives.
docker
colored-man-pages
sudo  # Double-Escape to toggle sudo
tarruda/zsh-autosuggestions

EOBUNDLES
antigen apply

unsetopt correct
setopt no_share_history

PROMPT='$(cs="%{" ce="%}" /opt/prompt/prompt)'
RPROMPT='$(cs="%{" ce="%}" /opt/prompt/rprompt "$?")'

# Never cache command completions
zstyle ":completion:*:commands" rehash 1


PATH="$HOME/bin:$HOME/.local/bin:$PATH"

alias ls="exa"
alias la="exa -a"
alias ll="exa --git -la"
alias l="ll"
alias lsl="ls"
alias sls="ls"
alias lsls="ls"
alias sl="ls"

alias qbin="curl https://beta.qbin.io -s -T"
transfer() { if [ $# -eq 0 ]; then echo -e "No arguments specified. Usage:\necho transfer /tmp/test.md\ncat /tmp/test.md | transfer test.md"; return 1; fi; tmpfile=$( mktemp -t transferXXX ); if tty -s; then basefile=$(basename "$1" | sed -e 's/[^a-zA-Z0-9._-]/-/g'); curl --progress-bar --upload-file "$1" "https://transfer.sh/$basefile" >> $tmpfile; else curl --progress-bar --upload-file "-" "https://transfer.sh/$1" >> $tmpfile ; fi; cat $tmpfile; rm -f $tmpfile; } 
alias pw='bash -c '"'"'echo `tr -dc $([ $# -gt 1 ] && echo $2 || echo "A-Za-z0-9") < /dev/urandom | head -c $([ $# -gt 0 ] && echo $1 || echo 30)`'"'"' --'
alias yq="python3 -c 'import sys, yaml, json; json.dump(yaml.load(sys.stdin), sys.stdout, indent=4)' | jq"

alias d="sudo -E docker"
alias dc="sudo -E docker-compose"

export EDITOR="/usr/local/bin/micro"

if [ `id -u` -eq 0 ]; then 
    start=`tput setab 1; tput setaf 7; tput bold`
    end=`tput setab 9; tput setaf 9; tput sgr0`
    echo
    echo "  $start                                                                       $end"
    echo "  $start  WARNING: You are in a root shell. This is probably a very bad idea.  $end"
    echo "  $start                                                                       $end"
    echo
fi

# print tip of the day
tput setaf 6 # cyan
echo -n "Tip of the day: "
shuf -n 1 /etc/tip-of-the-day.txt
tput setaf 9 # reset
