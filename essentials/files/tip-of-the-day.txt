Use "bat" instead of "cat" for features like syntax highlighting or git integration.
Use "fd" instead of "find" for a faster and more user-friendly search tool.
Use "hexyl" to display binary files.
Use "micro" instead of "nano" for mouse support and more intuitive keyboard shortcuts.
Use "mycli", "pgcli" and "litecli" to interactively access databases from the command line.
Use "ag" instead of "grep" to search file contents more intuitively.
Use "mosh" instead of "ssh" from your local machine if you're on an unstable internet connection.
Use "sudo nethogs" to show network usage by process.
Use "croc send {file}" for simple file transfer between systems.
Use "jq" and "yq ... < {file}" to query fields from JSON or YAML files.
Use "gron" to format JSON files in a greppable format.
Use "transfer {file}" to quickly share a file with someone.
Use "qbin {file}" or "... | qbin -" to upload a text file to https://qbin.io.
Use "pw [length]" to generate a secure and random password.
Place executable files in "~/.local/bin" or "~/bin" to add them to your PATH.